#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QMessageBox>
#include <QFileDialog>
#include <QFile>
#include <QDebug>
#include <QTextStream>
#include <QTextDocument>

#define STATUS_MESSAGE_TIMEOUT 5000 //milliseconds

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    _currentFileName(QString())
{
    ui->setupUi(this);

    //By default the file modification indicator must be invisible
    ui->fileModifInd->setVisible(false);

    // Set connections between slots and menus
    QObject::connect(ui->menuBar, &MenuBar::newFileTriggered, this, &MainWindow::_newFile);
    QObject::connect(ui->menuBar, &MenuBar::openFileTriggered, this, &MainWindow::_openFile);
    QObject::connect(ui->menuBar, &MenuBar::saveFileTriggered, this, &MainWindow::_saveFile);
    QObject::connect(ui->menuBar, &MenuBar::exitTriggered, this, &MainWindow::close);

    // Connect
    connect(ui->fileTextEdit->document(), &QTextDocument::modificationChanged,
                  this, &MainWindow::_fileWasModified);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::_newFile(void) {
    //1 : Check if the displaying has been saved
    if(this->_isFileModified()) {
        this->_saveFile();
        return;
    }
    //2 : Set the fileTitle to "NewFile.md"
    ui->statusBar->showMessage(tr("New file created."),STATUS_MESSAGE_TIMEOUT);
    return;
}

void MainWindow::_openFile(void) {
    //1 : Check if the displaying has been saved
    if(this->_isFileModified()) {
        this->_saveFile();
        return;
    }
    //2: Open a file dialog
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Open file"), "", tr("Mardown Files (*.md)"));
    qDebug() << __FUNCTION__ << "selected filename : " << fileName;
    //3: Open file fileName
    QFile file;
    file.setFileName(fileName);
    if (!file.open(QFile::ReadOnly | QFile::Text)) {
        QMessageBox::warning(this, tr("NotebooQ"),
                             tr("Cannot read file %1:\n%2.")
                             .arg(QDir::toNativeSeparators(fileName),
                                  file.errorString()));
        ui->statusBar->showMessage(tr("Failed to read %1:%2.")
                                   .arg(QDir::toNativeSeparators(fileName),
                                        file.errorString()),STATUS_MESSAGE_TIMEOUT);
        return;
    }
    //4: Load the selected file into the fileTextEdit widget
    QTextStream in(&file);
#ifndef QT_NO_CURSOR
    QApplication::setOverrideCursor(Qt::WaitCursor);
#endif
    ui->fileTextEdit->setPlainText(in.readAll());
#ifndef QT_NO_CURSOR
    QApplication::restoreOverrideCursor();
#endif

    _currentFileName = fileName;
    ui->fileTitle->setText(_currentFileName);
    ui->statusBar->showMessage(tr("File %1 opened.").arg(QDir::toNativeSeparators(fileName)),STATUS_MESSAGE_TIMEOUT);
    return;
}

void MainWindow::_saveFile(void) {
    QFile file;
    //1: If a new file (how to check it ?) -> Open a Newdir dialog
    if(_currentFileName.isEmpty()) {
        QString fileName = QFileDialog::getSaveFileName(this,
                                                        tr("Save file"), "newfile.md" , tr("Mardown Files (*.md)"));
        qDebug() << __FUNCTION__ << "selected filename : " << fileName;
        //1.1: Open file fileName
        file.setFileName(fileName);
        if (!file.open(QFile::WriteOnly | QFile::Text)) {
            QMessageBox::warning(this, tr("NotebooQ"),
                                 tr("Cannot write file %1:\n%2.")
                                 .arg(QDir::toNativeSeparators(fileName),
                                      file.errorString()));
            ui->statusBar->showMessage(tr("Failed to save %1:%2.")
                                       .arg(QDir::toNativeSeparators(fileName),
                                            file.errorString()),STATUS_MESSAGE_TIMEOUT);
            return;
        }
        //1.2: Copy data into this file
        QTextStream out(&file);
#ifndef QT_NO_CURSOR
        QApplication::setOverrideCursor(Qt::WaitCursor);
#endif
        qDebug() << __FUNCTION__ << "text to copy : " << ui->fileTextEdit->toPlainText();
        out << ui->fileTextEdit->toPlainText();
#ifndef QT_NO_CURSOR
        QApplication::restoreOverrideCursor();
#endif

        _currentFileName = fileName;
        ui->fileTitle->setText(_currentFileName);
        ui->fileTextEdit->document()->setModified(false);
        ui->statusBar->showMessage(tr("File saved."),STATUS_MESSAGE_TIMEOUT);
    }
    else {
    //2: Not a new file
        //2.1: Open file fileName
        file.setFileName(_currentFileName);
        if (!file.open(QFile::WriteOnly | QFile::Text)) {
            QMessageBox::warning(this, tr("NotebooQ"),
                                 tr("Cannot write file %1:\n%2.")
                                 .arg(QDir::toNativeSeparators(_currentFileName),
                                      file.errorString()));
            ui->statusBar->showMessage(tr("Failed to save %1:%2.")
                                       .arg(QDir::toNativeSeparators(_currentFileName),
                                            file.errorString()),STATUS_MESSAGE_TIMEOUT);
            return;
        }
        //2.2: Copy data into the file
        ui->fileTitle->setText(_currentFileName);
        ui->fileTextEdit->document()->setModified(false);
        ui->statusBar->showMessage(tr("File saved."),STATUS_MESSAGE_TIMEOUT);
    }
    return;
}

bool MainWindow::_isFileModified(void) {
    return ui->fileTextEdit->document()->isModified();
}

void MainWindow::_fileWasModified(void) {
    qDebug() << __FUNCTION__ << "Is file modified : " << _isFileModified();
    //Display the mark to identify that the file has been modified
    ui->fileModifInd->setVisible(_isFileModified());
}
