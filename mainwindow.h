#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "menubar.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    bool _isFileModified(void);

    QString _currentFileName;

    QWidget* _fileModifiedIndicator;

private slots:

    void _newFile(void);
    void _openFile(void);
    void _saveFile(void);

    void _fileWasModified(void);

};

#endif // MAINWINDOW_H
