#include "menubar.h"

MenuBar::MenuBar(QWidget *parent) :
    QMenuBar(parent)
{
    //Create File actions
    newFileAction = new QAction(tr("&New"), this);
    newFileAction->setShortcuts(QKeySequence::New);
    newFileAction->setStatusTip(tr("Create a new file"));
    connect(newFileAction, &QAction::triggered, this, &MenuBar::newFileTriggered);

    openFileAction = new QAction(tr("&Open..."), this);
    openFileAction->setShortcuts(QKeySequence::Open);
    openFileAction->setStatusTip(tr("Open an existing file"));
    connect(openFileAction, &QAction::triggered, this, &MenuBar::openFileTriggered);

    saveFileAction = new QAction(tr("&Save"), this);
    saveFileAction->setShortcuts(QKeySequence::Save);
    saveFileAction->setStatusTip(tr("Save the document to disk"));
    connect(saveFileAction, &QAction::triggered, this, &MenuBar::saveFileTriggered);

    exitAction = new QAction(tr("&Exit"), this);
    exitAction->setShortcuts(QKeySequence::Quit);
    exitAction->setStatusTip(tr("Quit NotebooQ application"));
    connect(exitAction, &QAction::triggered, this, &MenuBar::exitTriggered);

    //Create Edit actions
    undoAction = new QAction(tr("&Undo"), this);
    undoAction->setShortcuts(QKeySequence::Undo);
    undoAction->setStatusTip(tr("Undo the last operation"));
    connect(undoAction, &QAction::triggered, this, &MenuBar::undoTriggered);

    redoAction = new QAction(tr("&Redo"), this);
    redoAction->setShortcuts(QKeySequence::Redo);
    redoAction->setStatusTip(tr("Redo the last operation"));
    connect(redoAction, &QAction::triggered, this, &MenuBar::redoTriggered);

    cutAction = new QAction(tr("Cu&t"), this);
    cutAction->setShortcuts(QKeySequence::Cut);
    cutAction->setStatusTip(tr("Cut the current selection's contents to the "
                            "clipboard"));
    connect(cutAction, &QAction::triggered, this, &MenuBar::cutTriggered);

    copyAction = new QAction(tr("&Copy"), this);
    copyAction->setShortcuts(QKeySequence::Copy);
    copyAction->setStatusTip(tr("Copy the current selection's contents to the "
                             "clipboard"));
    connect(copyAction, &QAction::triggered, this, &MenuBar::copyTriggered);

    pasteAction = new QAction(tr("&Paste"), this);
    pasteAction->setShortcuts(QKeySequence::Paste);
    pasteAction->setStatusTip(tr("Paste the clipboard's contents into the current "
                              "selection"));
    connect(pasteAction, &QAction::triggered, this, &MenuBar::pasteTriggered);

    //Create Help actions
    aboutAction = new QAction(tr("&About"), this);
    aboutAction->setStatusTip(tr("Show the application's About box"));
    connect(aboutAction, &QAction::triggered, this, &MenuBar::aboutTriggered);

    //Create File menu
    fileMenu = this->addMenu(tr("&File"));
    fileMenu->addAction(newFileAction);
    fileMenu->addAction(openFileAction);
    fileMenu->addAction(saveFileAction);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAction);

    //Create Edit menu
    editMenu = this->addMenu(tr("&Edit"));
    editMenu->addAction(undoAction);
    editMenu->addAction(redoAction);
    editMenu->addSeparator();
    editMenu->addAction(cutAction);
    editMenu->addAction(copyAction);
    editMenu->addAction(pasteAction);
    editMenu->addSeparator();

    //Create Help menu
    helpMenu = this->addMenu(tr("&Help"));
    helpMenu->addAction(aboutAction);
}

MenuBar::~MenuBar(){}
