#ifndef MENUBAR_H
#define MENUBAR_H

#include <QMenuBar>

#include <QMenu>
#include <QAction>

class MenuBar : public QMenuBar
{
    Q_OBJECT

public:
    explicit MenuBar(QWidget *parent = nullptr);
    ~MenuBar();

private:
    QMenu* fileMenu;
    QMenu* editMenu;
    QMenu* helpMenu;

    // File menu actions
    QAction* newFileAction;
    QAction* openFileAction;
    QAction* saveFileAction;
    QAction* exitAction;

    // Edit menu actions
    QAction* undoAction;
    QAction* redoAction;
    QAction* cutAction;
    QAction* pasteAction;
    QAction* copyAction;

    // Help menu actions
    QAction* aboutAction;

signals:
    //File menu triggers
    void newFileTriggered(bool checked = false);
    void openFileTriggered(bool checked = false);
    void saveFileTriggered(bool checked = false);
    void exitTriggered(bool checked = false);

    //Edit menu triggers
    void undoTriggered(bool checked = false);
    void redoTriggered(bool checked = false);
    void cutTriggered(bool checked = false);
    void pasteTriggered(bool checked = false);
    void copyTriggered(bool checked = false);

    //Help menu triggers
    void aboutTriggered(bool checked = false);
};

#endif // MENUBAR_H
